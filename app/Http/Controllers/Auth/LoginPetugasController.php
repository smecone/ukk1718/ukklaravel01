<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Petugas;
use Illuminate\Support\Facades\Hash;

class LoginPetugasController extends Controller
{
    //
    public function index(Request $request){
      return view('page.auth.loginPetugas');
    }
    public function attempt(Request $request){
      $input_username = $request->username;
      $input_password = $request->password;
      $finded_user = Petugas::where('username',$input_username)->first();
      if(is_null($finded_user)){
        return 'failed user';
      }else{
        $true_password =$finded_user->password;
        if(Hash::check($input_password,$true_password)){
          session()->put('username',$input_username);
          session()->put('nama_petugas',$finded_user->nama_petugas);
          session()->put('petugas_id',$finded_user->id);
          return 'success';
        }else{
          return 'failed';
        }
      }
    }
    public function logout(Request $request){
      $request->session()->flush();
    }
}
