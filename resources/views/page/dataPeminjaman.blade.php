<?php use App\Http\Controllers\PeminjamanBukuController;?>
@extends('layouts.main',['pageName' => 'Pinjam Buku'])
@section('content')

  <table class="table table-hover">
  <tr>
   <th>No</th>
   <th>Nama Buku</th>
   <th>
     Nama Siswa
   </th>
   <th>Tanggal Pinjam</th>
   <th> Tanggal Kembali
   </th>
   <th>
     Denda
   </th>

  </tr>

@foreach ($data as $key)

  <tr>
      <td> {{$key->id}} </td>
      <td> {{$key->buku->nama_buku}} </td>
      <td> {{$key->siswa->nama_siswa}} </td>
      <td> {{$key->tgl_pinjam}} </td>
      <td> {{$key->tgl_kembalian}} </td>
      <td>{{PeminjamanBukuController::checkDenda($key->tgl_pinjam,$key->tgl_kembalian)}} </td>
      <td>
      <a href="#" class="btn btn-info btn-sm"> Edit </a>
      <a href="#" class="btn btn-danger btn-sm"> Hapus </a>
      <a href="{{route('prosesPengembalian', ['peminjaman'=>$key->id])}}" class="btn btn-danger btn-sm"> Hapus </a>
      </td>
  </tr>



@endforeach
  </table>
@endsection
