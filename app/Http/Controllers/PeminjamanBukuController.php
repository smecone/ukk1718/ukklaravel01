<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\PeminjamanBuku;
use App\Petugas;
use App\Buku;
use App\Siswa;
use App\PengembalianBuku;

class PeminjamanBukuController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Buku $buku)
    {
      return view('page.pinjamBuku')->with('buku',$buku);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Buku $buku)
    {
        //
        $waktu_kembalian = 3;
        $tgl_sekarang = Carbon::now(7);
        $tgl_kembalian = Carbon::now(7)->addDays($waktu_kembalian);
        $peminjaman = new PeminjamanBuku;
        $peminjaman->tgl_pinjam = $tgl_sekarang;
        $peminjaman->tgl_kembalian = $tgl_kembalian;
        $peminjaman->buku_id = $buku->id;
        $peminjaman->siswa_id = $request->siswa_id;
        $peminjaman->petugas_id = 1;
        $peminjaman->save();
        return 'Success';
    }
    public function dataPeminjaman(){
      $data = PeminjamanBuku::all();
      return view('page.dataPeminjaman',['data' => $data]);
    }

    public function detailIndex(Peminjaman $peminjaman){
      $peminjaman->denda = Carbon::now()->diffInDays(Carbon::parse($peminjaman->tgl_kembalian));
      // $peminjaman->denda = $peminjaman->denda * 500;
      $denda = $peminjaman->denda * 500;
      $pengembalian = new PengembalianBuku;
      $pengembalian->denda = $denda;
      // print_r($peminjaman->denda);
      return view('page.pengembalianBuku')->with(['peminjaman' => $peminjaman,'pengembalian' => $pengembalian]);
    }

    public static function checkDenda($tgl_pinjam,$tgl_kembalian){
      //$m_tgl_kembalian = Carbon::createFromFormat('Y-m-d',Carbon::parse($tgl_kembalian));
      $perbedaan_tanggal = Carbon::parse($tgl_kembalian)->diffInDays(Carbon::today());
      $denda = $perbedaan_tanggal * 500;
      return $denda;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PeminjamanBuku  $peminjaman
     * @return \Illuminate\Http\Response
     */
    public function show(Peminjaman $peminjaman)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PeminjamanBuku  $peminjaman
     * @return \Illuminate\Http\Response
     */
    public function edit(Peminjaman $peminjaman)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PeminjamanBuku  $peminjaman
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Peminjaman $peminjaman)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PeminjamanBuku  $peminjaman
     * @return \Illuminate\Http\Response
     */
    public function destroy(Peminjaman $peminjaman)
    {
        //
    }
}
