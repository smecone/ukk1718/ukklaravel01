<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Index
Route::get('/','SiswaController@index')->name('index')->middleware('session');

// Auth
Route::namespace('Auth')->group(function() {
  Route::prefix('login')->group(function() {
    Route::get('petugas','LoginPetugasController@index')->name('loginPetugas');
    Route::post('petugas','LoginPetugasController@attempt')->name('loginPetugasVerification');
  });

  Route::prefix('logout')->group(function() {
    Route::get('petugas','LoginPetugasController@logout')->middleware('session')->name('logoutPetugas');
  });

  Route::prefix('register')->group(function() {
    //Route::middleware(['session'])->group(function() {
      Route::get('petugas','RegisterPetugasController@index')->name('registerPetugas');
      Route::post('petugas','RegisterPetugasController@store')->name('prosesRegisterPetugas');
    //});
  });
});

// Buku
Route::group(['prefix'=>'buku', 'middleware'=>['session']], function() {
  Route::get('/','BukuController@index')->name('buku');
  Route::get('tambah','BukuController@create')->name('tambahBuku');
  Route::post('tambah','BukuController@store')->name('prosesTambahBuku');
});

// Siswa
Route::group(['prefix'=>'siswa', 'middleware'=>['session']], function() {
  Route::get('/','SiswaController@index')->name('siswa');
  Route::get('tambah', 'SiswaController@create')->name('tamabahSiswa');
  Route::post('tambah', 'SiswaController@store')->name('prosesTambahSiswa');
});

// Petugas
Route::group(['prefix'=>'petugas', 'middleware'=>['session']], function() {
  Route::get('/', 'PetugasController@index')->name('petugas');
});

// Peminjaman
Route::group(['prefix'=>'peminjaman', 'middleware'=>['session']], function() {
  Route::get('/data','PeminjamanBukuController@dataPeminjaman')->name('dataPeminjaman');
  Route::get('/data/{peminjaman}','PeminjamanBukuController@detailIndex')->name('detailPeminjaman');
  Route::get('pinjambuku/{buku}','PeminjamanBukuController@index')->name('pinjamBuku');
  Route::post('pinjambuku/{buku}','PeminjamanBukuController@store')->name('prosesPinjamBuku');
});

// Pengembalian
Route::group(['prefix'=>'peminjaman', 'middleware'=>['session']], function() {
  Route::post('data/peminjaman/{peminjaman}/kembali','PengembalianBukuController@store')->name('prosesPengembalian');
});
