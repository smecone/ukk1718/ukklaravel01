<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PeminjamanBuku extends Model
{
    //
      protected $table = "peminjaman";
      public $denda;
      public $timestamps = false;
      public function siswa(){
        return $this->belongsTo('App\Siswa');
      }
      public function petugas(){
        return $this->belongsTo('App\Petugas');
      }
      public function buku(){
        return $this->belongsTo('App\Buku');
      }
}
