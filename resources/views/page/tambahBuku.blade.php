@extends('layouts.main',['pageName' => 'Form User'])
@section('content')
  <form class="form-horizontal" action="{{route('prosesTambahBuku')}}" method="post">
    {{ csrf_field() }}
    {{ method_field('post') }}
  <fieldset>

  <div class="form-group">
    <label class="col-md-4 control-label" for="txtnama">Nama</label>
    <div class="col-md-5">
    <input id="txtnama" name="nama_buku" type="text" placeholder="Nama" class="form-control input-md" required="">
    </div>
  </div>


  <!-- Select Basic -->


  <!-- Button -->
  <div class="form-group">
    <label class="col-md-4 control-label" for="btnsimpan"></label>
    <div class="col-md-4">
      <button id="btnsimpan" name="btnsimpan" class="btn btn-primary">Simpan</button>
    </div>
  </div>

  </fieldset>
  </form>
  </div>
@endsection
