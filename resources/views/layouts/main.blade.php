@include('layouts.header')
<!-- INI HEADER -->
<header>
  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="{{route('index')}}"> PERPUSTAKAAN </a>
    <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
      <ul class="navbar-nav mr-auto">
      </ul>
      <form class="form-inline mt-2 mt-md-0">
        @if (session()->has('username'))
          <a class="btn btn-danger btn-sm my-2 my-sm-0" href="{{route('logoutPetugas')}}">Logout</a>
        @else
          <a class="btn btn-success btn-sm my-2 my-sm-0" href="{{route('loginPetugas')}}">Login</a>
        @endif
      </form>
    </div>
  </nav>
</header>
<!-- INI END HEADER -->

<!-- INI NAVIGASI -->
<main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">
<div class="container-fluid">
  <div class="row">
    <nav class="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar">
      <ul class="nav nav-pills flex-column">
        <li class="nav-item">
          <a class="nav-link active" href="#"> Dashboard <span class="sr-only">(current)</span></a>
        </li>
        <hr/>
        <li class="nav-item">
          <a class="nav-link" href="{{route('buku')}}"> Daftar Buku <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('siswa')}}"> Daftar Siswa <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('petugas')}}"> Daftar Petugas <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('tambahBuku')}}"> Tambah Buku <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('tamabahSiswa')}}"> Tambah Siswa <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('registerPetugas')}}">Tambah Petugas</a>
        </li>
        <hr/>
        <li class="nav-item">
          <a class="nav-link" href="#"> Tambah Peminjaman <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('dataPeminjaman')}}"> Daftar Peminjaman <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#"> Lakukan Pengembalian <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#"> Daftar Pengembalian <span class="sr-only">(current)</span></a>
        </li>
      </ul>
    </nav>
  </div>
</div>
<!-- INI END NAVIGASI -->

<!-- INI KONTEN -->
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="#">Page</a></li>
<li class="breadcrumb-item active">{{$pageName}}</li>
</ol>

@if (session('status'))
  <p>
    {{session('status')}}
  </p>
@endif

{{-- isi kontent --}}
@yield('content')
<!-- INI END KONTEN -->

</main>
@include('layouts.footer')
