<?php

namespace App\Http\Controllers;

use App\PengembalianBuku;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Peminjaman;
class PengembalianBukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Peminjaman $peminjaman,PengembalianBuku $pengembalian)
    {
      // var_dump($peminjaman);
      //print_r($peminjaman->siswa->nama_siswa);
        //apc_bin_load
        $denda = Carbon::parse($peminjaman->tgl_kembalian)->diffInDays(Carbon::now());
        $denda = $denda * 500;
        print_r($denda);
        $pengembalianBuku = new PengembalianBuku;
        $pengembalianBuku->tgl_kembali = Carbon::now();
        $pengembalianBuku->denda = $denda;
        $pengembalianBuku->id_peminjaman = $peminjaman->id;
        $pengembalianBuku->save();
        return 'Success';
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PengembalianBuku  $pengembalianBuku
     * @return \Illuminate\Http\Response
     */
    public function show(PengembalianBuku $pengembalianBuku)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PengembalianBuku  $pengembalianBuku
     * @return \Illuminate\Http\Response
     */
    public function edit(PengembalianBuku $pengembalianBuku)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PengembalianBuku  $pengembalianBuku
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PengembalianBuku $pengembalianBuku)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PengembalianBuku  $pengembalianBuku
     * @return \Illuminate\Http\Response
     */
    public function destroy(PengembalianBuku $pengembalianBuku)
    {
        //
    }
}
