@extends('layouts.main',['pageName' => 'Pinjam Buku'])
@section('content')
  <form class="form-horizontal" action="{{route('prosesPengembalian',['peminjaman' => $peminjaman])}}" method="post">
    {{ csrf_field() }}
    {{ method_field('post') }}
  <fieldset>

  <div class="form-group">
    <h1>Pengembalian Buku</h1>
    <h4>Detail Peminjaman :</h4>
  </br>
    <h6>Nama Peminjam : {{$peminjaman->siswa->nama_siswa}} </h6>
    <h6>Nama Buku : {{$peminjaman->buku->nama_buku}} </h6>
    <h6>Denda : {{$pengembalian->denda}}</h6>
  </div>


  <!-- Select Basic -->


  <!-- Button -->
  <div class="form-group">
    <label class="col-md-4 control-label" for="btnsimpan"></label>
    <div class="col-md-4">
      <button id="btnsimpan" name="btnsimpan" class="btn btn-primary">Proses</button>
    </div>
  </div>

  </fieldset>
  </form>
  </div>
@endsection
