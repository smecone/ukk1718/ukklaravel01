@extends('layouts.main',['pageName'=>'Index User'])

@section('content')

  <table class="table table-hover">
  <tr>
   <th>No</th>
   <th>Nama Buku</th>

   <th>Action</th>
  </tr>

@foreach ($bukus as $key)

  <tr>
      <td> {{$key['id']}} </td>
      <td> {{$key['nama_buku']}} </td>
      <td>
      <a href="{{route('pinjamBuku',['buku'=>$key])}}" class="btn btn-info btn-sm"> Edit </a>
      <a href="#" class="btn btn-danger btn-sm"> Hapus </a>
      </td>
  </tr>



@endforeach
  </table>

@endsection
