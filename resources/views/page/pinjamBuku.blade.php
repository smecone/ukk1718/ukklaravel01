@extends('layouts.main',['pageName' => 'Pinjam Buku'])
@section('content')
  <form class="form-horizontal" action="{{route('prosesPinjamBuku',['buku' => $buku])}}" method="post">
    {{ csrf_field() }}
    {{ method_field('post') }}
  <fieldset>

  <div class="form-group">
    <label class="col-md-4 control-label" for="txtnama">Nama</label>
    <div class="col-md-5">
    <input id="txtnama" name="siswa_id" type="text" placeholder="Nama" class="form-control input-md" required="">
    </div>
  </div>


  <!-- Select Basic -->


  <!-- Button -->
  <div class="form-group">
    <label class="col-md-4 control-label" for="btnsimpan"></label>
    <div class="col-md-4">
      <button id="btnsimpan" name="btnsimpan" class="btn btn-primary">Simpan</button>
    </div>
  </div>

  </fieldset>
  </form>
  </div>
@endsection
