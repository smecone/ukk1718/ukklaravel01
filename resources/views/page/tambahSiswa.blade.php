@extends('layouts.main',['pageName' => 'Form User'])
@section('content')
  <form class="form-horizontal" action="{{route('prosesTambahSiswa')}}" method="post">
    {{ csrf_field() }}
    {{ method_field('post') }}
  <fieldset>

  <div class="form-group">
    <label class="col-md-4 control-label" for="txtnama">Nama</label>
    <div class="col-md-5">
    <input id="txtnama" name="nama_siswa" type="text" placeholder="Nama" class="form-control input-md" required="">
    </div>
  </div>

  <!-- Text input-->
  <div class="form-group">
    <label class="col-md-4 control-label" for="txtusername">Username</label>
    <div class="col-md-5">
    <input id="txtusername" name="username" type="text" placeholder="Username" class="form-control input-md" required="">
    </div>
  </div>

  <!-- Select Basic -->


  <!-- Button -->
  <div class="form-group">
    <label class="col-md-4 control-label" for="btnsimpan"></label>
    <div class="col-md-4">
      <button id="btnsimpan" name="btnsimpan" class="btn btn-primary">Simpan</button>
    </div>
  </div>

  </fieldset>
  </form>
  </div>
@endsection
