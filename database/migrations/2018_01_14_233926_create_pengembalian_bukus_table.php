<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengembalianBukusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengembalian_bukus', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('tgl_kembali')->nullable();
            $table->integer('denda')->nullable();
            $table->integer('id_peminjaman')->unsigned()->index()->nullable();
            $table->foreign('id_peminjaman')->references('id')->on('peminjaman');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::dropIfExists('pengembalian_bukus');
    }
}
