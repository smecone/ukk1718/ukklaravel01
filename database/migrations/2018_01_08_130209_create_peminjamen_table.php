<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeminjamenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peminjaman', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('tgl_pinjam');
            $table->dateTime('tgl_kembalian');
            $table->integer('buku_id')->unsigned()->index()->nullable();
            $table->integer('siswa_id')->unsigned()->index()->nullable();
            $table->integer('petugas_id')->unsigned()->index()->nullable();

            $table->foreign('buku_id')->references('id')->on('bukus');
            $table->foreign('siswa_id')->references('id')->on('siswas');
            $table->foreign('petugas_id')->references('id')->on('petugas');
            $table->integer('status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('pengembalian_bukus');
      Schema::drop('peminjaman');
      Schema::drop('petugas');
      Schema::drop('bukus');
      Schema::drop('siswas');



    }
}
